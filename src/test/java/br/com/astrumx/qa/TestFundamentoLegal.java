package br.com.astrumx.qa;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatusCode;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import br.com.astrumx.qa.model.ControleDeAcesso;
import br.com.astrumx.qa.model.FundamentoLegalDTOResponse;
import br.com.astrumx.qa.model.LoginRequest;
import br.com.astrumx.qa.model.LoginResponse;

@SpringBootTest
class TestFundamentoLegal extends AbstractAstrumXQA {
	
	private static final String URI_FUNDAMENTOS_LEGAIS_DTO = "/fundamentos-legais-dto";
	private static final String URI_FUNDAMENTOS_LEGAIS = "/fundamentos-legais";


	@Test
	void whenGetFundamentosLegaisDTO_thenListaOK() {
		var listaDTO = new ArrayList<FundamentoLegalDTOResponse>();

		try {
			LoginRequest request = createLoginRequest();
			
			LoginResponse loginResponse = doLogin(request);
			
			var controle = new ControleDeAcesso( loginResponse.organizacoes().get(0) );
			
			LocalDateTime inicio = LocalDateTime.now();
			
			var fluxFL = super.buildWebClient()
				.get()
				.uri(URI_FUNDAMENTOS_LEGAIS_DTO)
				.attribute("__controle", controle)
				.cookie("astrum_token", loginResponse.token() )
				.retrieve()
				.bodyToFlux( FundamentoLegalDTOResponse.class )
				;
			
			fluxFL.subscribe( f -> listaDTO.add(f) );
			
			fluxFL.blockLast();
			
			assertThat( listaDTO ).isNotEmpty();
			
			
			if (DEBUG_MODE) {
				LocalDateTime termino = LocalDateTime.now();
				
				System.out.println("Fundamentos legais DTO: ");
				listaDTO.forEach( System.out::println );
				
				System.out.print("Size: ");
				System.out.println( listaDTO.size() );
				
				
				Duration duracao = Duration.between(inicio, termino);
				
				long segundos = duracao.toSeconds();
				System.out.print("Duração em s: ");
				System.out.println( segundos );
				
				long milisegundos = duracao.toMillis();
				System.out.print("Duração em ms: ");
				System.out.println( milisegundos );
			}
			
		
		}catch(Exception e) {
			if (e instanceof WebClientResponseException) {
				var aux = (WebClientResponseException)e;
				String responseBody = aux.getResponseBodyAsString();
				System.out.println("Response body: ");
				System.out.println( responseBody );
				HttpStatusCode statsCode = aux.getStatusCode();
				System.out.println("Status code: ");
				System.out.println( statsCode );
			}
			e.printStackTrace();
			fail( e );
		}
	}

	
	@Test
	void whenGetFundamentosLegais_thenListaOK() {
		var listaFL = new ArrayList<FundamentoLegalDTOResponse>();

		try {
			LoginRequest request = createLoginRequest();
			
			LoginResponse loginResponse = doLogin(request);
			
			var controle = new ControleDeAcesso( loginResponse.organizacoes().get(0) );
			
			LocalDateTime inicio = LocalDateTime.now();
			
			var fluxFL = super.buildWebClient()
				.get()
				.uri(URI_FUNDAMENTOS_LEGAIS)
				.attribute("__controle", controle)
				.cookie("astrum_token", loginResponse.token() )
				.retrieve()
				.bodyToFlux( FundamentoLegalDTOResponse.class );
			
			fluxFL.subscribe( f -> listaFL.add(f) );
			
			fluxFL.blockLast();
			
			assertThat( listaFL ).isNotEmpty();

			if (DEBUG_MODE) {
				LocalDateTime termino = LocalDateTime.now();
				
				System.out.println("Fundamentos legais: ");
				listaFL.forEach( System.out::println );
				
				System.out.print("Size: ");
				System.out.println( listaFL.size() );
				
				
				Duration duracao = Duration.between(inicio, termino);
				
				long segundos = duracao.toSeconds();
				System.out.print("Duração em s: ");
				System.out.println( segundos );
				
				long milisegundos = duracao.toMillis();
				System.out.print("Duração em ms: ");
				System.out.println( milisegundos );
			}
			
		
		}catch(Exception e) {
			if (e instanceof WebClientResponseException) {
				var aux = (WebClientResponseException)e;
				String responseBody = aux.getResponseBodyAsString();
				System.out.println("Response body: ");
				System.out.println( responseBody );
				HttpStatusCode statusCode = aux.getStatusCode();
				System.out.println("Status code:");
				System.out.println( statusCode );
			}
			e.printStackTrace();
			fail( e );
		}
	}
	

}
