package br.com.astrumx.qa.model;

import java.util.Date;

public record ElementoDoPlanoDeClassificacaoDTO(
		long id,
		int nivel,
		long idNoPia,
		boolean reaproveitavel,
		boolean bloqueado,
		long versaoDoPlanoQuePublicou,
		String situacao,
		long idReferencia,
		String tabelaReferencia,
		String valorReferencia,
		Date dataCriacao,
		boolean valido,
		boolean modificado
		
		) {
}


/*
id": 141724,
"nivel": 1,
"idNoPai": -1,
"codigo": "01",
"": false,
"": false,
"versaoDoPlanoQuePublicou": -1,
"situacao": "ADICIONADO",
"agrupamentoHorizontal": null,
"idReferencia": 1107,
"tabelaReferencia": "TB_ASTRUM_PROCESSOS",
"informacoesExtras": null,
"valorReferencia": "Gestão - excluir",
"dataCriacao": 1674442800000,
"valido": true,
"modificado": true
*/