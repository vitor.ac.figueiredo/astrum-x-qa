package br.com.astrumx.qa.model;

public record LoginRequest(
		String baseUrl,
		String email,
		String login,
		String senha,
		Integer idOrg
		) {

}
