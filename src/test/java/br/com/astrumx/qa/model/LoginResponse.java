package br.com.astrumx.qa.model;

import java.util.List;

public record LoginResponse(
		String sigla,
		String tarefas,
		String token,
		List<Organizacao> organizacoes
		) {
	

}
