package br.com.astrumx.qa.model;

public record FundamentoLegalDTOResponse(
	    long id,	  
	    String nome
		) {
}
