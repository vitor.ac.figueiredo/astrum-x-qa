package br.com.astrumx.qa.model;

/*
SELECT 
linha.idreferencia
, linha.codigo
, linha.situacao
, linha.valorreferencia
, linha.tabelareferencia
, length(linha.codigo) - length(replace(linha.codigo, '.', '')) + 1
, (SELECT CAST(count(*) AS INTEGER) FROM relatorios.tb_relatorios_elementos_do_plano LEFT JOIN relatorios.tb_relatorios_planos_de_classificacao pc_ ON pc_.id = linha.planodeclassificacao_id WHERE codigo LIKE linha.codigo || '.%' AND planodeclassificacao_id = pc.id)
, (SELECT CAST(count(*) AS INTEGER) FROM astrum.tb_astrum_tipos_de_processo WHERE ORGANIZACAO_ID = " + organizacao.getId() + ") AS A
, md.identificador
, cfg.titulodinamico
, md.valor
, grp.id AS GRP_ID
, grp.nome AS GRP_NOME
, cfg.nome AS CFG_NOME
, grp.fase2
, (SELECT count(*) FROM astrum.tb_astrum_metadados cfg WHERE cfg.flagtabelatemporalidade) AS num_metadados\n" +
 */

public record TemporalidadeDTO(
		  Object linhaIdReferencia
		, Object linhaCodigo
		, Object linhaSituacao
		, Object linhaValorReferencia
		, Object linhaTabelaReferencia
		, Object tamanhoCodigo
		, Object qtdElementos //(SELECT CAST(count(*) AS INTEGER) FROM relatorios.tb_relatorios_elementos_do_plano LEFT JOIN relatorios.tb_relatorios_planos_de_classificacao pc_ ON pc_.id = linha.planodeclassificacao_id WHERE codigo LIKE linha.codigo || '.%' AND planodeclassificacao_id = pc.id)
		, Object qtdTiposProcesso //(SELECT CAST(count(*) AS INTEGER) FROM astrum.tb_astrum_tipos_de_processo WHERE ORGANIZACAO_ID = " + organizacao.getId() + ") AS A
		, Object mdIdentificador
		, Object mdTitulodinamico
		, Object mdValor
		, Object grpId // AS GRP_ID
		, Object grpNome //AS GRP_NOME
		, Object cfgNome ///AS CFG_NOME
		, Object grpFase2
		, Object qtdMdDeTT //(SELECT count(*) FROM astrum.tb_astrum_metadados cfg WHERE cfg.flagtabelatemporalidade) AS num_metadados\n" +
		
		) {

}
