package br.com.astrumx.qa.model;

import java.util.Date;
import java.util.List;

public record PlanoDeClassificacaoDTO(
		long id,
		long versao,
		String apelido,
		String situacao,
		Date dataCriacao,
		boolean valido,
		List<ElementoDoPlanoDeClassificacaoDTO> elementos
		
		) {

}


/*
 {
    "id": 78,
    "versao": 0,
    "apelido": null,
    "situacao": "EM_ATUALIZACAO",
    "dataCriacao": 1715050800000,
    "dataFixacao": null,
    "dataPublicacao": null,
    "dataRevogacao": null,
    "dataCancelamento": null,
    "dataExclusao": null,
    "valido": true,
    "elementos": [
 */
