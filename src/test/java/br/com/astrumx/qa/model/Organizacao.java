package br.com.astrumx.qa.model;

public record Organizacao(
				 Long id,
				 String sigla,
				 String codigo,
				 String nome,
				 String chave,
				 String emailInstitucional,
				 boolean situacao,
				 Integer totalDeDigitosPlanoDeClassificacao
				) {

}
