package br.com.astrumx.qa;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.reflect.Array;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import br.com.astrumx.qa.model.ControleDeAcesso;
import br.com.astrumx.qa.model.LoginRequest;
import br.com.astrumx.qa.model.LoginResponse;
import br.com.astrumx.qa.model.PlanoDeClassificacaoDTO;
import br.com.astrumx.qa.model.TemporalidadeDTO;

/**
 * Teste para Relatorio de Temporalidade
 * @author vitor.figueiredo
 * @since 24 out 2024
 */
@SpringBootTest
public class TestTemporalidade extends AbstractAstrumXQA {
	
	//URI testadas:
	
	final String URI_RELATORIO_TEMPORALIDADE2_PELO_ID = "/relatorio/tabela-de-temporalidade2/{id}";
	
	
	@Test//Positivo
	void dadoPlanoClassificacaoIdValido_whenGetRelatorioTabelaDeTemporalidade2PeloId_thenOK() {
		long planoClassificacaoIdValido = 78;
		
		LoginRequest loginRequest = createLoginRequest();
		
		LoginResponse loginResponse = doLogin(loginRequest);

		ControleDeAcesso controle = new ControleDeAcesso( loginResponse.organizacoes().get(0) );
		
		String uriPronta = URI_RELATORIO_TEMPORALIDADE2_PELO_ID.replace("{id}", String.valueOf(planoClassificacaoIdValido ));
		
		ArrayList response = super.buildWebClient()
			.get()
			.uri( uriPronta )
			.attribute("__controle", controle)
			.cookie("astrum_token", loginResponse.token() )
			.retrieve()
			.bodyToMono( ArrayList.class )
			.block();
		
		
		printDetalhes(response);
		
		assertThat( response ).isNotNull();
		assertThat( response ).isNotEmpty();
	}



	//auxiliar...
	
	private void printDetalhes(ArrayList response) {
		if (DEBUG_MODE) {
			for (Object o : response) {
				ArrayList linhaArrayList = (ArrayList) o;
				
				Object[] linhaArr = (Object[])linhaArrayList.toArray();
				
				TemporalidadeDTO temporalidadeDTO = new TemporalidadeDTO(
						 linhaArr[0]
						,linhaArr[1]
						,linhaArr[2]
						,linhaArr[3]
						,linhaArr[4]
						,linhaArr[5]
						,linhaArr[6]
						,linhaArr[7]
						,linhaArr[8]
						,linhaArr[9]
						,linhaArr[10]		
						,linhaArr[11]
						,linhaArr[11]
						,linhaArr[12]
						,linhaArr[13]
						,linhaArr[14]
				);
				
				System.out.println( temporalidadeDTO );
			}
		}
	}

	
	
	@Test//Negativo
	void dadoPlanoClassificacaoIdINVALIDO_whenGetRelatorioTabelaDeTemporalidade2PeloId_thenExpectStatusIsOKComRespostaVazia() {
		long planoClassificacaoIdINVALIDO = 999;
		
		LoginRequest loginRequest = createLoginRequest();
		
		LoginResponse loginResponse = doLogin(loginRequest);

		ControleDeAcesso controle = new ControleDeAcesso( loginResponse.organizacoes().get(0) );
		
		String uriPronta = URI_RELATORIO_TEMPORALIDADE2_PELO_ID.replace("{id}", String.valueOf(planoClassificacaoIdINVALIDO ));
		
		ArrayList responseBody = super.buildWebTestClient()
			.get()
			.uri( uriPronta )
			.attribute("__controle", controle)
			.cookie("astrum_token", loginResponse.token() )
			.exchange()
			.expectStatus()
				.is2xxSuccessful()
			.expectBody( ArrayList.class )
				.returnResult()
				.getResponseBody()
			;
		
		assertThat( responseBody ).isEmpty();
	}
	
	

}
