package br.com.astrumx.qa;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import br.com.astrumx.qa.model.ControleDeAcesso;
import br.com.astrumx.qa.model.ElementoDoPlanoDeClassificacaoDTO;
import br.com.astrumx.qa.model.LoginRequest;
import br.com.astrumx.qa.model.LoginResponse;
import br.com.astrumx.qa.model.PlanoDeClassificacaoDTO;
import reactor.core.publisher.Flux;

/**
 * Testes para Plano de Classificação
 * @author vitor.figueiredo
 * @since 18 set 2024
 */
@SpringBootTest
public class TestPlanoDeClassificacao extends AbstractAstrumXQA {
	
	//URI testadas:
	
	final String URI_RELATORIO_PLANOS_DE_CLASSIFICACAO = "/relatorio/planos-de-classificacao";
	
	final String URI_RELATORIO_PLANO_DE_CLASSIFICACAO2_PELO_ID = "/relatorio/plano-de-classificacao2/{id}";
	
	//final String URI_RELATORIO_PLANO_DE_CLASSIFICACAO3_PELO_ID = "/relatorio/plano-de-classificacao3/{id}";//terminar metodo

	final String URI_RELATORIO_PLANOS_DE_CLASSIFICAO_PELA_SITUACAO = "/relatorio/planos-de-classificacao/{situacao}";
	
	
	@Test
	void dadoSituacaoINVALIDA_whenGetPlanosDeClassificaoPelaSituacao_then404NotFound() {
		String situacaoValida = "SITUACAO_NAO_EXISTENTE";
		
		LoginRequest loginRequest = createLoginRequest();
		
		LoginResponse loginResponse = doLogin(loginRequest);

		ControleDeAcesso controle = new ControleDeAcesso( loginResponse.organizacoes().get(0) );
		
		String uriPronta = URI_RELATORIO_PLANOS_DE_CLASSIFICAO_PELA_SITUACAO.replace("{situacao}", situacaoValida);
		
		String responseBody = super.buildWebTestClient()
			.get()
			.uri( uriPronta )
			.attribute("__controle", controle)
			.cookie("astrum_token", loginResponse.token() )
			.exchange()
			.expectStatus()
				.isNotFound()
			.expectBody( String.class )
				.returnResult()
				.getResponseBody()
			;
		
		if (DEBUG_MODE) {
			System.out.println( responseBody );			
		}
	}
	
	
	
	@Test
	void dadoSituacaoValida_whenGetPlanosDeClassificaoPelaSituacao_thenListOK() {
		String situacaoValida = "EM_ATUALIZACAO";
		
		var listaPC = new ArrayList<PlanoDeClassificacaoDTO>();
		
		LoginRequest loginRequest = createLoginRequest();
		
		LoginResponse loginResponse = doLogin(loginRequest);

		ControleDeAcesso controle = new ControleDeAcesso( loginResponse.organizacoes().get(0) );
		
		String uriPronta = URI_RELATORIO_PLANOS_DE_CLASSIFICAO_PELA_SITUACAO.replace("{situacao}", situacaoValida);
		
		Flux<PlanoDeClassificacaoDTO> flux = super.buildWebClient()
			.get()
			.uri( uriPronta )
			.attribute("__controle", controle)
			.cookie("astrum_token", loginResponse.token() )
			.retrieve()
			.bodyToFlux( PlanoDeClassificacaoDTO.class );
		
		flux.subscribe(pc -> listaPC.add(pc) );
		
		flux.blockLast();
		
		assertThat( listaPC ).isNotEmpty();
	}
	
	
	@Test
	void dadoPlanoClassificacaoIdValido_whenGetPlanoDeClassificao2PeloId_thenOK() {
		long planoClassificacaoIdValido = 78;
		
		LoginRequest loginRequest = createLoginRequest();
		
		LoginResponse loginResponse = doLogin(loginRequest);

		ControleDeAcesso controle = new ControleDeAcesso( loginResponse.organizacoes().get(0) );
		
		String uriPronta = URI_RELATORIO_PLANO_DE_CLASSIFICACAO2_PELO_ID.replace("{id}", String.valueOf(planoClassificacaoIdValido ));
		
		PlanoDeClassificacaoDTO pc = super.buildWebClient()
			.get()
			.uri( uriPronta )
			.attribute("__controle", controle)
			.cookie("astrum_token", loginResponse.token() )
			.retrieve()
			.bodyToMono( PlanoDeClassificacaoDTO.class )
			.block();
		
		assertThat( pc ).isNotNull();
	}

	
	// TODO descobrir como passar o Map de dados
//	@Test
//	void dadoPlanoClassificacaoIdValido_whenGetPlanoDeClassificao3PeloId_thenOK() {
//		long planoClassificacaoIdValido = 78;
//		
//		LoginRequest loginRequest = createLoginRequest();
//		
//		LoginResponse loginResponse = doLogin(loginRequest);
//
//		ControleDeAcesso controle = new ControleDeAcesso( loginResponse.organizacoes().get(0) );
//		
//		String uriPronta = URI_RELATORIO_PLANO_DE_CLASSIFICACAO3_PELO_ID.replace("{id}", String.valueOf(planoClassificacaoIdValido ));
//		
//		PlanoDeClassificacaoDTO pc = super.buildWebClient()
//			.post()
//			.uri( uriPronta )
//			.attribute("__controle", controle)
//			.cookie("astrum_token", loginResponse.token() )
//			.retrieve()
//			.bodyToMono( PlanoDeClassificacaoDTO.class )
//			.block();
//		
//		assertThat( pc ).isNotNull();
//		
//		showPlanoDeClassificaoDTO( pc );
//	}
	
	



	@Test
	void dadoPlanoClassificacaoIdINVALIDO_whenGetPlanoDeClassificao2PeloId_thenErro404NotFound() {
		long planoClassificacaoIdINVALIDO = 999;
		
		
		LoginRequest loginRequest = createLoginRequest();
		
		LoginResponse loginResponse = doLogin(loginRequest);

		ControleDeAcesso controle = new ControleDeAcesso( loginResponse.organizacoes().get(0) );
		
		String uriPronta = URI_RELATORIO_PLANO_DE_CLASSIFICACAO2_PELO_ID.replace("{id}", String.valueOf(planoClassificacaoIdINVALIDO ));
		
		try {
			super.buildWebTestClient()
				.get()
				.uri( uriPronta )
				.attribute("__controle", controle)
				.cookie("astrum_token", loginResponse.token() )
				.exchange()
				.expectStatus()
					.isNotFound();
			
		} catch (WebClientResponseException e) {
			e.printStackTrace();
			fail("Devia ter lançado Exception com status 404");
		}
	}

	
	
	
	@Test
	void whenGetPlanosDeClassificao_thenListOK() {
		
		LoginRequest loginRequest = createLoginRequest();
		
		LoginResponse loginResponse = doLogin(loginRequest);

		ControleDeAcesso controle = new ControleDeAcesso( loginResponse.organizacoes().get(0) );
		
		var listaPC = new ArrayList<PlanoDeClassificacaoDTO>();
		
		var flux = super.buildWebClient()
				.get()
				.uri( URI_RELATORIO_PLANOS_DE_CLASSIFICACAO )
				.attribute("__controle", controle)
				.cookie("astrum_token", loginResponse.token() )
				.retrieve()
				.bodyToFlux( PlanoDeClassificacaoDTO.class );
			
		flux.subscribe( p -> listaPC.add(p) );
		
		flux.blockLast();
	
		assertThat( listaPC ).isNotEmpty();
		
		
		showListaPlanoDeClassificaoDTO(listaPC);
	}
	
	
	
	// Exemplo com WebTestClient
	@Test 
	void whenGetRelatorioPlanosDeClassificao_thenExpectStatusIsOK() {
		
		LoginRequest loginRequest = createLoginRequest();
		
		LoginResponse loginResponse = doLogin(loginRequest);

		ControleDeAcesso controle = new ControleDeAcesso( loginResponse.organizacoes().get(0) );
		
		super.buildWebTestClient()
				.get()
				.uri( URI_RELATORIO_PLANOS_DE_CLASSIFICACAO )
				.attribute("__controle", controle)
				.cookie("astrum_token", loginResponse.token() )
				.exchange()
				.expectStatus()
					.isOk()
				;
		
	}
	
	
	// auxiliares
	// **********
	
	private void showListaPlanoDeClassificaoDTO(List<PlanoDeClassificacaoDTO> listaPC) {
		if (DEBUG_MODE) {		
			System.out.print("Qtd de PC: ");
			System.out.println( listaPC.size() );
	
			if (listaPC.size() > 0) {
				PlanoDeClassificacaoDTO pc1 = listaPC.get(0);
				
				showPlanoDeClassificaoDTO( pc1 );
			}
		}
	}

	
	private void showPlanoDeClassificaoDTO(PlanoDeClassificacaoDTO pc) {
		if (DEBUG_MODE) {
			
			List<ElementoDoPlanoDeClassificacaoDTO> elementos = pc.elementos();
			System.out.print("Qtd de Elementos primeiro PC: ");
			System.out.println( elementos.size()  );
			
			if (elementos.size() > 0) {
				System.out.println( "Elementos: " );
				elementos.forEach( System.out::println );
			}
		}
	}

	
}
