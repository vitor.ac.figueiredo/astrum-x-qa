package br.com.astrumx.qa;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import br.com.astrumx.qa.model.LoginRequest;
import br.com.astrumx.qa.model.LoginResponse;

@SpringBootTest
public class TestLogin extends AbstractAstrumXQA {

	
	@Test
	void dadoCredencias_qdoLogin_entaoOK() {
		LoginRequest request = createLoginRequest();
		
		LoginResponse response = doLogin(request);
		
		assertThat( response ).isNotNull();

		if (DEBUG_MODE) {				
			System.out.println( response );
			System.out.println( response.token() );
		}
	}
	
	
}
