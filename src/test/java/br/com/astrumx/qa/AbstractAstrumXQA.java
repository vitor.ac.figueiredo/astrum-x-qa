package br.com.astrumx.qa;

import org.springframework.test.web.reactive.server.WebTestClient;

import org.springframework.web.reactive.function.client.WebClient;

import br.com.astrumx.qa.model.LoginRequest;
import br.com.astrumx.qa.model.LoginResponse;

public abstract class AbstractAstrumXQA {

	final boolean DEBUG_MODE = false; //usar apenas para testes pontuais
	
	//localhost transpetro
	private String baseUrl = "http://localhost:8079";
	private String email = "astrum@issx";
	private String login = "astrum@issx";
	private String senha = "123456";
	private Integer idOrg = 1;//transpetro
	
	
	//homolog petrobras
//	private String uri = "https://app09.virtuaserver.com.br/astrum";
//	private String email = "astrum@petrobras";
//	private String login = "astrum@petrobras";
//	private String senha = "123456";
//	private Integer idOrg = 4;//petrobras
	
	
	
	
	//utils
	
	protected LoginResponse doLogin(LoginRequest loginRequest) {
		
		LoginResponse response = WebClient.create( loginRequest.baseUrl() )
			.post()
			.uri("/login")
			.bodyValue( loginRequest )
			.retrieve()
			.bodyToMono( LoginResponse.class )
			.block()
			;
		
		return response;
	}
	
	
	protected LoginRequest createLoginRequest() {
		return new LoginRequest(
				this.baseUrl,
				this.email, 
				this.login, 
				this.senha, 
				this.idOrg
				);
		
	}
	
	protected WebClient buildWebClient() {
		return WebClient.builder()
		.baseUrl( this.baseUrl )
		.codecs( conf -> conf
				.defaultCodecs()
				.maxInMemorySize( 16 * 1024 * 1024 ))
		.build()
		;
	}
	
	
	protected WebTestClient buildWebTestClient() {
		return WebTestClient.bindToServer()
		.codecs( conf -> conf
				.defaultCodecs()
				.maxInMemorySize( 16 * 1024 * 1024 ))
		.baseUrl( this.baseUrl )
		.build();
		
	}
	
	
}
