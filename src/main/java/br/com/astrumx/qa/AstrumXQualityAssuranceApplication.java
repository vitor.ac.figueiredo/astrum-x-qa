package br.com.astrumx.qa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AstrumXQualityAssuranceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AstrumXQualityAssuranceApplication.class, args);
	}

}
